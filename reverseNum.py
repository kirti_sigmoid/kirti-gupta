class reverseNumber:
    def __init__(self, num=1):
        self.num = num
        self.rev = 0

    def revers_digits(self):
        if(self.num<0):
            raise ValueError("Number cannot be less than 0")


        n = self.num
        self.rev = 0
        while (n > 0):
            a = n % 10
            self.rev = self.rev * 10 + a
            n = n // 10
        return self.rev

    def add_num(self, s):
        self.num = s

    def read_from_file(self, filename):
        file = open(filename, "r")
        line = file.readline()
        return line


if __name__ == "__main__":
    num=int(input("Enter your number : "))
    obj1 = reverseNumber(num)
    print(obj1.revers_digits())


